BITS 64
SECTION .text

; define main as globale, which is recognizable by the linker  
global main

main:
	jmp short data
code:
	; print message
	mov rdi, 1 ; file descriptor
	pop rsi
	mov rdx, 16 ; message length
	mov rax, 1 ; syscall number for write
	syscall
	
	; exit application
	mov rdi, 0 ; error number
	mov rax, 60 ; syscall number for exit
	syscall
data:
	call code
	message db "Hello students!", 10, 13, 0
